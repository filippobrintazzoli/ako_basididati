package utility;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

public final class AlertPopup {

    public static void showAlert(String title, String header, String content) {
        Alert alert = new Alert(AlertType.WARNING);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(content);
        alert.showAndWait();
    }
    
}
