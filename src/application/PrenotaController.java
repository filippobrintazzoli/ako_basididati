package application;

import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ComboBoxBase;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.util.Callback;
import utility.AlertPopup;

public class PrenotaController {

    @FXML
    private ToggleGroup lingua;
    
    @FXML
    private ToggleButton spagnolo;

    @FXML
    private ToggleButton portoghese;

    @FXML
    private ToggleButton italiano;

    @FXML
    private ToggleButton inglese;
    
    @FXML
    private ToggleButton russo;
    
    @FXML
    private ToggleButton tedesco;
    
    @FXML
    private ComboBox<String> insegnante;
    
    @FXML
    private DatePicker data;
    
    @FXML
    private ComboBox<Integer> ora;
    
    @FXML
    private Button conferma;
    
    @FXML
    private Label visualizza;
    
    private List<String> mails = null;
    private List<String> names = null;

    private final List<Integer> oreTot = IntStream.range(9, 22).boxed().collect(Collectors.toList());
    
    private List<Integer> oreEffettive = new ArrayList<>();;
    
    private static String studentEmail = null;
    private String insegnanteEmail = null;
    
    @FXML
    void linguaPressed(ActionEvent event) {
        this.resetComponent(insegnante);
        this.resetComponent(data);
        this.resetComponent(ora);
        this.insegnante.getItems().clear();
        this.ora.getItems().clear();
        this.data.setDisable(true);
        this.ora.setDisable(true);
        this.conferma.setDisable(true);
        if (lingua.getSelectedToggle() == null) {
            this.insegnante.setDisable(true);
        } else {
            this.insegnante.setDisable(false);
            InsegnantiTable insegnanti = new InsegnantiTable();
            try {
                mails = insegnanti.getMailsFromLingua(this.getLingua());
                names = insegnanti.getNamesFromMails(mails);
            } catch (ClassNotFoundException e) {
                AlertPopup.showAlert("Errore", "Non è stato possibile caricare i driver per la connessione al database", "'classNotFoundException'");
                e.printStackTrace();
            } catch (SQLException e) {
                AlertPopup.showAlert("Errore", "Non è stato possibile collegarsi al database, riprova", "'SQLException'");
                e.printStackTrace();
            }
            this.insegnante.setItems(FXCollections.observableArrayList(names));
        }
    }
    
    @FXML
    void insegnantePressed(ActionEvent event) {
        insegnanteEmail = mails.get(names.indexOf(insegnante.getValue()));
        this.resetComponent(data);
        this.resetComponent(ora);
        this.ora.getItems().clear();
        this.ora.setDisable(true);
        this.conferma.setDisable(true);
        final Callback<DatePicker, DateCell> dayCellFactory = 
                new Callback<DatePicker, DateCell>() {
            @Override
            public DateCell call(final DatePicker calendar) {
                return new DateCell() {
                    @Override
                    public void updateItem(LocalDate item, boolean empty) {
                        super.updateItem(item, empty);

                        if (item.isBefore(LocalDate.now().plusDays(1))) {
                            setDisable(true);
                            setStyle("-fx-background-color: #ffc0cb;");
                        }
                    }
                };
            }
        };
        this.data.setDayCellFactory(dayCellFactory);
        this.data.setDisable(false);
    }
    
    @FXML
    void dataPressed(ActionEvent event) {
        this.ora.getItems().clear();
        this.ora.setDisable(false);
        this.conferma.setDisable(true);
        List<Integer> oreOccupate = null;
        oreEffettive.clear();
        oreEffettive.addAll(oreTot);
        LezioniTable lezioni = new LezioniTable();
        try {
            oreOccupate = lezioni.getHoursFromDate(data.getValue(), studentEmail, insegnanteEmail);
        } catch (ClassNotFoundException e) {
            AlertPopup.showAlert("Errore", "Non è stato possibile caricare i driver per la connessione al database", "'classNotFoundException'");
            e.printStackTrace();
        } catch (SQLException e) {
            AlertPopup.showAlert("Errore", "Non è stato possibile collegarsi al database, riprova", "'SQLException'");
            e.printStackTrace();
        }
        oreEffettive.removeAll(oreOccupate);
        this.ora.setItems(FXCollections.observableArrayList(oreEffettive));
    }
    
    @FXML
    void oraPressed(ActionEvent event) {
        this.conferma.setDisable(false);
    }
    
    @FXML
    void onConfirmation(ActionEvent event) {
        LezioniTable lezioni = new LezioniTable();
        try {
            lezioni.nuovaLezione(studentEmail, insegnanteEmail, data.getValue(), ora.getValue(), this.getLingua());
        } catch (ClassNotFoundException e) {
            AlertPopup.showAlert("Errore", "Non è stato possibile caricare i driver per la connessione al database", "'classNotFoundException'");
            e.printStackTrace();
        } catch (SQLException e) {
            AlertPopup.showAlert("Errore", "Non è stato possibile collegarsi al database, riprova", "'SQLException'");
            e.printStackTrace();
        }
        AlertPopup.showAlert("Avviso", "Prenotazione avvenuta con successo!", "Conferma per continuare");
        this.resetComponent(ora);
        this.resetComponent(data);
        this.resetComponent(insegnante);
        this.conferma.setDisable(true);
        this.ora.setDisable(true);
        this.data.setDisable(true);
        this.insegnante.setDisable(true);
        this.lingua.selectToggle(null);
    }
    
    @FXML
    void onVisualizzaClicked() throws IOException {
        Main.getSceneController().showVisualizza(studentEmail);
    }
    
    static public void setEmail(String mail) {
        studentEmail = mail;
    }
    
    private String getLingua() {
        Toggle t = this.lingua.getSelectedToggle();
        if (t == italiano) {
            return "italiano";
        } else if (t == inglese) {
            return "inglese";
        } else if (t == portoghese) {
            return "portoghese";
        } else if (t == spagnolo) {
            return "spagnolo";
        } else if (t == russo) {
            return "russo";
        } else if (t == tedesco) {
            return "tedesco";
        } else {
            return "";
        }
    }
    
    private void resetComponent(ComboBoxBase<?> n) {
        EventHandler<ActionEvent> handler = n.getOnAction();
        n.setOnAction(null);
        n.setValue(null);
        n.setOnAction(handler);
    }

}
