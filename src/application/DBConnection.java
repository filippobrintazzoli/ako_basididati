package application;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {

    public Connection getAccessConnection() throws SQLException, ClassNotFoundException {
        Connection connection = null;
        Class.forName("net.ucanaccess.jdbc.UcanaccessDriver");
        connection = DriverManager.getConnection("jdbc:ucanaccess://res\\AKO_AccessDatabase.accdb");
        return connection;
    }

}
