package application;

import java.io.IOException;
import java.sql.SQLException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import utility.AlertPopup;

public class LoginController {

    @FXML
    private PasswordField password;

    @FXML
    private Label warningLabel;

    @FXML
    private Label registrati;

    @FXML
    private TextField email;

    @FXML
    private Button conferma;

    @FXML
    void onConfirmation(ActionEvent event) throws IOException {
        if (!this.emailCorretta()) {
            this.warningLabel.setText("Inserisci email valida");
            this.warningLabel.setVisible(true);
        } else if (!this.allFieldsFilled()) {
            this.warningLabel.setText("Riempi tutti i campi");
            this.warningLabel.setVisible(true);
        } else {
            this.warningLabel.setVisible(false);
            StudentiTable stud = new StudentiTable();
            try {
                if (stud.login(email.getText(), password.getText())) {
                    Main.getSceneController().showPrenota(email.getText());
                } else {
                    AlertPopup.showAlert("Errore", "Qualcosa è andato storto", "Non esistono corrispondenze, controlla i dati o registrati");
                }
            } catch (ClassNotFoundException e) {
                AlertPopup.showAlert("Errore", "Non è stato possibile caricare i driver per la connessione al database", "'classNotFoundException'");
                e.printStackTrace();
            } catch (SQLException e) {
                AlertPopup.showAlert("Errore", "Non è stato possibile collegarsi al database, riprova", "'SQLException'");
                e.printStackTrace();
            }
        }
    }

    @FXML
    void onRegistratiClicked() throws IOException {
        Main.getSceneController().showRegister();
    }

    private boolean allFieldsFilled() {
        return !(email.getText().length() == 0 || password.getText().length() == 0);
    }

    // Controlla se la mail inserita è corretta
    private boolean emailCorretta() {
        String email = this.email.getText();
        String regex = "^[\\w!#$%&'*+/=?`{|}~^-]+(?:\\.[\\w!#$%&'*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }
}
