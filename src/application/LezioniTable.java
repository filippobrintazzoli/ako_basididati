package application;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class LezioniTable {

    DBConnection dataSource = new DBConnection();

    public List<Integer> getHoursFromDate(LocalDate date, String mailStudente, String mailInsegnante) throws ClassNotFoundException, SQLException {
        List<Integer> hours = new ArrayList<>();
        Connection connection = this.dataSource.getAccessConnection();
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery("SELECT ora FROM Lezioni WHERE data=#" + date + "# AND (mailStudente='" + mailStudente + "' OR mailInsegnante='"+ mailInsegnante +"')");
        while (resultSet.next()) {
            hours.add(resultSet.getInt(1));
        }
        statement.close();
        connection.close();
        return hours;
    }

    public void nuovaLezione(String mailStudente, String mailInsegnante, LocalDate data, int ora, String lingua) throws ClassNotFoundException, SQLException {
        Connection connection = this.dataSource.getAccessConnection();
        Statement statement = connection.createStatement();
        String info = "'" + mailStudente + "', '" + mailInsegnante + "', #" + data + "#, " + ora + ", '" + lingua + "'";
        statement.executeUpdate("INSERT INTO Lezioni (mailStudente, mailInsegnante, data, ora, lingua) VALUES ("+ info +")");
        statement.close();
        connection.close();
    }
    
    public List<String> lezioniPrenotateDa(String mail) throws ClassNotFoundException, SQLException {
        List<String> list = new ArrayList<>();
        Connection connection = this.dataSource.getAccessConnection();
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery("SELECT data, ora, lingua, mailStudente, mailInsegnante FROM Lezioni WHERE mailStudente='" + mail + "' ORDER BY data");
        while (resultSet.next()) {
            String s = resultSet.getDate(1) + "|" + resultSet.getInt(2) + "\t\t\t\t" + resultSet.getString(3) + "\t\t\t\t" + resultSet.getString(4) + "\t\t\t\t" + resultSet.getString(5);
            list.add(s);
        } return list;
    }

}
