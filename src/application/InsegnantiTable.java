package application;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class InsegnantiTable {

    DBConnection dataSource = new DBConnection();

    public List<String> getMailsFromLingua(String lingua) throws ClassNotFoundException, SQLException {
        List<String> mails = new ArrayList<>();
        Connection connection = this.dataSource.getAccessConnection();
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery("SELECT email FROM Insegnanti WHERE lingua1='" + lingua + "' OR lingua2='" + lingua + "'");
        while (resultSet.next()) {
            mails.add(resultSet.getString(1));
        }
        statement.close();
        connection.close();
        return mails;
    }

    public List<String> getNamesFromMails(List<String> mails) throws ClassNotFoundException, SQLException {
        List<String> names = new ArrayList<>();
        Connection connection = this.dataSource.getAccessConnection();
        Statement statement = connection.createStatement();
        mails.forEach(x -> {
            try {
                ResultSet resultSet = statement.executeQuery("SELECT nome, cognome FROM Insegnanti WHERE email='" + x + "'");
                if (resultSet.next()) {
                    names.add(resultSet.getString(1) + " " + resultSet.getString(2));
                }
            } catch (SQLException e) {
                System.out.println("Fallita l'esecuzione della query");
                e.printStackTrace();
            }
        });
        statement.close();
        connection.close();
        return names;
    }

}
