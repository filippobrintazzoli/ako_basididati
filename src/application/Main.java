package application;

import javafx.application.Application;
import javafx.stage.Stage;


public class Main extends Application {
    
    private static SceneController c;
    
    @Override
    public void start(Stage primaryStage) throws Exception {
          c = new SceneController(primaryStage);
    }

    public static void main(String[] args) {
        launch(args);
    }
    
    public static SceneController getSceneController() {
        return c;
    }
}
