package application;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javafx.event.ActionEvent;
import utility.AlertPopup;
import utility.CFGenerator;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

public class RegistratiController {

    private CFGenerator codiceFiscale;

    @FXML
    private TextField nome;

    @FXML
    private TextField cognome;

    @FXML
    private TextField comune;

    @FXML
    private DatePicker dataDiNascita;

    @FXML
    private ComboBox<String> sesso;

    @FXML
    private TextField email;

    @FXML
    private PasswordField password;

    @FXML
    private Label warningLabel;

    @FXML
    private Label login;

    @FXML
    private Button conferma;
    
    @FXML
    void onConfirmation(ActionEvent event) throws IOException {
        if (!this.comuneCorretto()) {
            this.warningLabel.setText("Comune non corretto");
            this.warningLabel.setVisible(true);
        } else if (!this.emailCorretta()) {
            this.warningLabel.setText("Inserisci email valida");
            this.warningLabel.setVisible(true);
        } else if (!this.allFieldsFilled()) {
            this.warningLabel.setText("Inserisci tutti i campi");
            this.warningLabel.setVisible(true);
        } else {
            this.warningLabel.setVisible(false);
            this.codiceFiscale = new CFGenerator(nome.getText(), cognome.getText(), comune.getText(), meseToString(dataDiNascita.getValue().getMonthValue()), dataDiNascita.getValue().getYear(), 07, sesso.getValue().toString());
            System.out.println(codiceFiscale.getCodiceFiscale());
            StudentiTable stud = new StudentiTable();
            try {
                stud.registra(codiceFiscale.getCodiceFiscale(), nome.getText(), cognome.getText(), email.getText(), password.getText());
                AlertPopup.showAlert("Avviso", "Registrazione avvenuta con successo!", "Verrai reindirizzato alla pagina di login");
                Main.getSceneController().showLogin();
            } catch (ClassNotFoundException e) {
                AlertPopup.showAlert("Errore", "Non è stato possibile caricare i driver per la connessione al database", "'classNotFoundException'");
                e.printStackTrace();
            } catch (SQLIntegrityConstraintViolationException e) {
                AlertPopup.showAlert("Errore", "Sembra che tu sia già registrato, controlla i dati o fai login", "'SQLIntegrityConstraintViolationException'");
                e.printStackTrace();
            } catch (SQLException e) {
                AlertPopup.showAlert("Errore", "Non è stato possibile collegarsi al database, riprova", "'SQLException' - E' possibile che tu sia già registrato.");
                e.printStackTrace();
            }
        }
    }

    @FXML
    void onLoginClicked() throws IOException {
        Main.getSceneController().showLogin();
    }

    // Check if all fields are filled
    private boolean allFieldsFilled() {
        return !(nome.getText().length() == 0 || cognome.getText().length() == 0 || comune.getText().length() == 0 || sesso.getValue() == null || dataDiNascita.getValue() == null || email.getText().length() == 0 || password.getText().length() == 0);
    }

    // Return the month string associated to the month number m
    private String meseToString(int m) {
        switch (m) {
        case 1: return "Gennaio";
        case 2: return "Febbraio";
        case 3: return "Marzo";
        case 4: return "Aprile";
        case 5: return "Maggio";
        case 6: return "Giugno";
        case 7: return "Luglio";
        case 8: return "Agosto";
        case 9: return "Settembre";
        case 10: return "Ottobre";
        case 11: return "Novembre";
        case 12: return "Dicembre";
        default: return "";
        }
    }

    // Restituisce se il comune inserito si trova nella lista dei comuni validi
    private boolean comuneCorretto() {
        try {
            Scanner scanner = new Scanner(new File("res/Comuni.txt"));
            scanner.useDelimiter("\r\n");
            String c = this.comune.getText();
            while(scanner.hasNext()) {
                String s1 = scanner.nextLine();
                String s2 = s1.substring(0,s1.indexOf('-')-1);
                if(s2.equalsIgnoreCase(c)) {
                    scanner.close();
                    return true;
                }
            }
            scanner.close();
        } catch(Exception e) {e.printStackTrace();}
        return false;
    }

    // Controlla se la mail inserita è corretta
    private boolean emailCorretta() {
        String email = this.email.getText();
        String regex = "^[\\w!#$%&'*+/=?`{|}~^-]+(?:\\.[\\w!#$%&'*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }
}
