package application;

import java.io.IOException;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class SceneController {
    
    Stage stage;
    
    public SceneController(Stage stage) {
        this.stage = stage;
        this.stage.setResizable(false);
//        try {
//            this.showPrenota("prova1@email.com");
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
        
        // Applicazione comincia mostrando la schermata di registrazione
        try {
            this.showRegister();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public void showRegister() throws IOException {
        Pane register = (Pane) FXMLLoader.load(getClass().getResource("registratiFXML.fxml"));
        stage.setScene(new Scene(register));
        stage.setTitle("AKO - Registrati");
        register.requestFocus();
        stage.centerOnScreen();
        stage.show();
    }
    
    public void showLogin() throws IOException {
        Pane login = (Pane) FXMLLoader.load(getClass().getResource("loginFXML.fxml"));
        stage.setScene(new Scene(login));
        stage.setTitle("AKO - Login");
        login.requestFocus();
        stage.centerOnScreen();
        stage.show();
    }
    
    public void showPrenota(String email) throws IOException {
        Pane prenota = (Pane) FXMLLoader.load(getClass().getResource("prenotaFXML.fxml"));
        stage.setScene(new Scene(prenota));
        stage.setTitle("AKO - Prenota");
        prenota.requestFocus();
        stage.centerOnScreen();
        stage.show();
        PrenotaController.setEmail(email);
    }
    
    public void showVisualizza(String email) throws IOException {
        Pane visualizza = (Pane) FXMLLoader.load(getClass().getResource("visualizzaFXML.fxml"));
        stage.setScene(new Scene(visualizza));
        stage.setTitle("AKO - Visualizza");
        visualizza.requestFocus();
        stage.centerOnScreen();
        stage.show();
        VisualizzaController.setEmail(email);
    }
    
}
