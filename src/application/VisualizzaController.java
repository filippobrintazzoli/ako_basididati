package application;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import utility.AlertPopup;

public class VisualizzaController {

    @FXML
    private Label prenota;

    @FXML
    private ListView<String> list;

    @FXML
    private Button conferma;

    private static String studentEmail = null;
    
    @FXML
    void onConfirmation(ActionEvent event) {
        list.getItems().clear();
        List<String> info = new ArrayList<>();
        String intestazione = "Data e ora\t\t\t\tLingua\t\t\t\tMail Studente\t\t\t\tMail Insegnante";
        LezioniTable lezioni = new LezioniTable();
        info.add(intestazione);
        try {
            info.addAll(lezioni.lezioniPrenotateDa(studentEmail));
        } catch (ClassNotFoundException e) {
            AlertPopup.showAlert("Errore", "Non è stato possibile caricare i driver per la connessione al database", "'classNotFoundException'");
            e.printStackTrace();
        } catch (SQLException e) {
            AlertPopup.showAlert("Errore", "Non è stato possibile collegarsi al database, riprova", "'SQLException'");
            e.printStackTrace();
        }
        list.setItems(FXCollections.observableArrayList(info));
    }

    @FXML
    void onPrenotaClicked() throws IOException {
        Main.getSceneController().showPrenota(studentEmail);
    }

    static public void setEmail(String mail) {
        studentEmail = mail;
    }
    
}
